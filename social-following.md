# Social following

[Related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/21582)

In implementing an ActivityPub actor, there are three distinct parts we need:

* We need to make a list of activities publicly available, similar to what
  a RSS feed is doing, but using the ActivityStream format.
* We need to add a "profile page" endpoint for the actor, providing its name
  and url to the outbox (list of activities) endpoint
* We need to accept external POST requests for subscribing, and then push
  our own requests to the subscriber when an event happen.

The last one can happen separately, in a distinct merge request, as after
the first two, it's usually already possible for an ActivityPub-based tool
to display the provided content (acting as a homepage for the actor).

There are 5 resources that would make good candidate for being actors:

* the user activities (all of project, group and releases activities)
* the project activities (17 activities, like creation, deletion, issues and merge requests events)
* the group activities (6 activities, like creation and joining of member events, + all of project activities)
* the topic feed (1 activity : project added to the topic)
* the project releases (1 activity : new release created)

This gives us a total of 10 merge requests:

* 5 to add profile and activities list for users, projects, groups, topics and releases
* 5 to add subscribing and pushing for the same resources

Although, the project activities are quite numerous, maybe it would be
better to create a MR for each activity. In which case, that would be 17 MR
for the project feed alone. There's a long road in front of us!

## What to begin with

What would feel more natural to both Gitlab and the fediverse would be to
start with the user and the project feeds. Those are the two most important
resources of Gitlab, and the fediverse is all about following people,
right?

But I don't think it would be the most efficient choice. What is our
priority, here? Is it to give people who are both active Gitlab users and
active fediverse users a mean to see in their fediverse app the timelines
they already see in Gitlab, or is it to bring new people from the
fediverse, by salting their social feed with useful news from projects on
Gitlab? If it's the second, maybe we should not give them as first contact
something that generates tens, maybe hundreds of events a day and that
won't be that useful if they're not really into a project (in that case,
they don't care to know that someone reviewed a MR or that a commit was
pushed in a branch).

If we want a good first contact between the fediverse and Gitlab, I think
we should start with releases. Someone casually interested in Gitlab will
certainly want to know a software they like has been released in a new
version, and see what changed in that version. Then we should implement the
feed for topics, so that they know when a new software has been released in
a topic they like.

Conveniently, starting with releases also allows to start the
implementation with an easier resource. Building a feed of created releases
is easier and simpler by several orders of magnitude than implementing a
user feed or a project feed with all the events it implies, so author and
reviewers can focus on the architecture without being flooded by LOC added.

After that, there is a good reason to implement the user feed in very
last : the user feed is basically an aggregation of all other feeds. User
activities are project activities (create repos, push, manage issues, merge
requests and wikis, etc), group activities (creating, joining, leaving a
group, etc) and releases activities. So it makes sense to build those first
before building the user feed. The same reasoning applies to the group
feed : groups activities includes their projects activities, so it should
be built after projects.

So here is the order I think we should go:

1. Add releases feed's profile and list
1. Add subscribing and pushing for releases actor
1. Add topic feed's profile and list
1. Add subscribing and pushing for topic actor
1. Add project activities' profile and list
1. Add subscribing and pushing for project actor
1. Add group activities' profile and list
1. Add subscribing and pushing for group actor
1. Add user activities' profile and list
1. Add subscribing and pushing for user actor

## Implementing profiles and lists

### Profile

A profile is implemented at the `resources#show` location, responding to
the Accept header `application/ld+json;
profile="https://www.w3.org/ns/activitystreams"` or `application/activity+json`.

> **In RoR**: the obvious way would be to add a mime-type handler in
> `config/initializers/mime_types.rb`, so we can then use it in controllers
> with `#respond_to`.
>
> It's not the way Mastodon went. They have a specialized ActivityPub
> module in which they put their ActivityPub endpoints, and the user
> profile document at `accounts#show` is just responding to `format.json`.
> But Gitlab already have a lot of json responders, we certainly don't want
> to mix those with ActivityPub documents.

The JSON response on an existing and publicly accessible resource should
read something like this:

```js
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "type": TYPE,
  "id": URL_OF_THE_RESOURCE,
  "name": RESOURCE_NAME,
  "outbox": URL_OF_THE_RESOURCE_OUTBOX,
  "inbox": null, // until implementing subscription
  "summary": OPTIONAL_DESCRIPTION, // this is interpreted as HTML
}
```

Type should be one [defined in the Activity
Vocabulary](https://www.w3.org/TR/activitystreams-vocabulary/#actor-types).

The previously mentioned resources could be mapped like this:

* user -> Person
* project -> Application
* group -> Group
* topic -> Group

### Activities list

A list is implemented at `GET /<resource>/outbox`, using the same Accept
headers.

Each type of resource will have their specific values, but here is what
they all have in common:

```js
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "type": "OrderedCollection",
  "id": URL_OF_THE_RESOURCE,
  "totalItems": N,
  "first": URL_OF_FIRST_PAGE,
  "last": URL_OF_LAST_PAGE
}
```

`first` and `last` point to a double linked list used for pagination.

```js
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "type": "OrderedCollectionPage",
  "id": URL_OF_THE_PAGE,
  "next": URL_OF_NEXT_PAGE,
  "prev": URL_OF_PREVIOUS_PAGE,
  "partOf": URL_OF_THE_OUTBOX,
  "orderedItems": [
    // ...
  ]
}
```

The various Gitlab event actions that will be used for all actors are
defined in [the Event model](https://gitlab.com/gitlab-community/gitlab/-/blob/554c64e5afd2aef8aa65787e59855fe1319eb7b6/app/models/event.rb#L16-29).

Here are the various event's actions supported by Gitlab, and what they
could be mapped to as ActivityStream's Activity:

* created: [Create](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-create) or [Add](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-add) (depending on whether it's added _to_ something else)
* updated: [Update](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-update)
* closed: [Remove](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-remove)
* reopened: [Update](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-update)
* pushed: [Add](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-add)
* commented: [Add](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-add)
* merged: [Accept](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-accept)
* joined: [Join](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-join)
* left: [Leave](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-leave)
* destroyed: [Delete](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-delete)
* expired: [Leave](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-leave)
* approved: [Accept](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-accept)

Actors:

* [Activities for release actor](./social-following/release.md)
* [Activities for topic actor](./social-following/topic.md)
* [Activities for user actor](./social-following/user.md)
* [Activities for project actor](./social-following/project.md)
* [Activities for group actor](./social-following/group.md)

## Implementing subscribing and pushing

Subscribing is implemented at `POST /<resource>/inbox`. The workflow is as
follow:

1. A Follow activity is received in `POST /<resource>/inbox`.
1. The activity provides the name and the address of the following actor, we store it.
1. When sends an Accept or Reject activity to the follower's inbox.
1. If the follower's inbox can't be reached after a number attempts (we
   decide how many), their subscription is removed.

Then, when an activity happens, it should issue requests to each
subscriber's inbox to notify them of the activity:

1. An activity generates a background job
1. That job creates a job for each subscriber
1. That second job POST on follower's `/inbox`.
1. If the follower's inbox can be reached after a number attempts (we
   decide how many), their subscription is removed.

The standard specifies that we should implement a `GET
/<resource>/followers` endpoint to list who is subscribed, "should" meaning
we can avoid it if we have a very good reason. I think Gitlab qualifies,
here, as it's not a social network and exposing who follow this or that
project could be a privacy issue (granted, RSS would be a better choice for
the users who want privacy there). It can still be implemented later if
people want it.

Worth noting is that this url (`POST /<resource>/inbox`) is not meant only
for subscribing, but to send all activities meant for this actor, so the
endpoint may be used for other things later.

### Concrete rails implementation

There are a few things to plan, here, as it is not a simple endpoint like
the profile and feed mentioned previously.

* Each resource has a `POST /inbox` endpoint
  * It should validate the activity received, both its format and checking
    the user mentioned is not already subscribed
  * It should add a follow to the resource, so we need a new table and
    model (ActivityPub::Follower)
  * It should queue a job to issue back an Accept activity (if everything
    went right) or a Reject activity (if the user is already subscribed)
* The responder worker (ActivityPub::FollowResponse?) posts its activity
  (Accept or Reject) to the subscriber inbox
  * if it can't reach the inbox, it retries later
  * after x number of retries, it gives up and deletes the
    ActivityPub::Follower
* For each resource, the services or hooks creating the related events
  should be identified.
* When related events are created, a job must be queued for each
  ActivityPub::Follower for the related resource.
* This push worker (ActivityPub::Push? or maybe we'll need one per
  resource) posts the related Activity to the subscriber inbox
  * if it can't reach the inbox, it retries later
  * after x number of retries, it gives up and deletes the
    ActivityPub::Follower

Reference : part of the standard [describing the Follow
activity](https://www.w3.org/TR/activitypub/#follow-activity-inbox).

I think it's possible to have a single model for all resource followers :
ActivityPub::Follower. The model can then use a polymorphic `belongs_to`,
like Event, to reference the actual resource. Given this follow table just
contains the name of the user and their inbox address, it would be overkill
to add a new table for each resource we want to serve through ActivityPub.
