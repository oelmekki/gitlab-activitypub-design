# Cross instance search

> What is present here is just collected ideas, this part has not been
> properly designed yet.

## Discovery of instances

Starting with this feature, we'll need to decide how we are going to handle
federation. The current way most fediverse software work is that they allow
any other instance to post content by default, and it's expected from the
instance owner to do all the moderation work to filter out spam and various
abuses. When an instance turns out to not do that work or not do it well
enough, other instances blacklist it ("defederate" from it).
Unsurprisingly, spam is a recurring problem on the fediverse, just like it
is in emails with SMTP, an other protocol allowing unauthorized posting.

To avoid imposing on Gitlab to hire a team of moderators and because abuses
when code is involved can quickly escalate to nefarious consequences, we
think it would be better to explicitly add the instances that will be
considered as "federated". At the point of this design document in the
implementation process, it will mean the instances in the whitelist will be
the ones contacted to perform the search and who are allowed to ask for
searches. Later, it will be the ones that are allowed open cross-instance
merge requests and issue cross-instance comments, etc. If Gitlab figure a
way to do moderation properly, it could be turned into a blacklist instead,
but better start too safe than not enough.

How to add a new instance? They could simply open an issue to ask to be
added.

## Managing federated instances

We need an admin collection of features to CRUD the list of authorized
instances.

## Performing a search

Once we have a list of trusted instance, we can just perform a round robin
search on their API, updating the search interface with new results as they
come.

## Authorization

How should authorization be managed? Do we create a oauth token for the
neighbor instance? This would create friction, as now there must be
specific configuration on both instance, twice if they both add each other
as federated instance. Maybe we could just rely on reverse dns for the IP
that contacted us, to see if it matches the domain of a whitelisted
instance?

