# Activities for following user actor

## Profile

That one is easy because it's basically the first resource ActivityPub has
in mind.

```js
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "id": USER_PROFILE_URL,
  "type": "Person",
  "name": USER_NAME,
  "url": USER_PROFILE_URL,
  "outbox": USER_OUTBOX_URL,
  "inbox": null,
}
```

## Outbox

The user actor is special because as a god object, it can basically be
linked to all events happening on the platform.

It's basically a join of events on other resources:

* all release activities
* all project activities
* all group activities

