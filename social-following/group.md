# Activities for group actor

## Profile

```js
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "id": GROUP_URL,
  "type": "Group",
  "name": GROUP_NAME,
  "summary": GROUP_DESCRIPTION,
  "url": GROUP_URL,
  "outbox": GROUP_OUTBOX_URL,
  "inbox": null,
}
```

## Outbox

The various activities for a group are:

* the group was created
* all the project activities for projects in that group and its subgroups
* a user joined the group
* a user left the group
* the group was deleted
* a subgroup was created
* a subgroup was deleted

### the group was created

```js
{
  "id": GROUP_OUTBOX_URL#event_id,
  "type": "Create",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "actor": {
    "id": USER_PROFILE_URL,
    "type": "Person",
    "name": USER_NAME,
    "url": USER_PROFILE_URL,
  },
  "object": {
    "id": GROUP_URL,
    "type": "Group",
    "name": GROUP_NAME,
    "url": GROUP_URL,
  }
}
```

### a user joined the group

```js
{
  "id": GROUP_OUTBOX_URL#event_id,
  "type": "Join",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "actor": {
    "id": USER_PROFILE_URL,
    "type": "Person",
    "name": USER_NAME,
    "url": USER_PROFILE_URL,
  },
  "object": {
    "id": GROUP_URL,
    "type": "Group",
    "name": GROUP_NAME,
    "url": GROUP_URL,
  },
}
```

### a user left the group

```js
{
  "id": GROUP_OUTBOX_URL#event_id,
  "type": "Leave",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "actor": {
    "id": USER_PROFILE_URL,
    "type": "Person",
    "name": USER_NAME,
    "url": USER_PROFILE_URL,
  },
  "object": {
    "id": GROUP_URL,
    "type": "Group",
    "name": GROUP_NAME,
    "url": GROUP_URL,
  },
}
```

### the group was deleted

```js
{
  "id": GROUP_OUTBOX_URL#event_id,
  "type": "Delete",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "actor": {
    "id": USER_PROFILE_URL,
    "type": "Person",
    "name": USER_NAME,
    "url": USER_PROFILE_URL,
  },
  "object": {
    "id": GROUP_URL,
    "type": "Group",
    "name": GROUP_NAME,
    "url": GROUP_URL,
  }
}
```

### a subgroup was created

```js
{
  "id": GROUP_OUTBOX_URL#event_id,
  "type": "Create",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "actor": {
    "id": USER_PROFILE_URL,
    "type": "Person",
    "name": USER_NAME,
    "url": USER_PROFILE_URL,
  },
  "object": {
    "id": GROUP_URL,
    "type": "Group",
    "name": GROUP_NAME,
    "url": GROUP_URL,
    "context": {
    "id": PARENT_GROUP_URL,
    "type": "Group",
    "name": PARENT_GROUP_NAME,
    "url": PARENT_GROUP_URL,
    }
  }
}
```

### a subgroup was deleted

```js
{
  "id": GROUP_OUTBOX_URL#event_id,
  "type": "Delete",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "actor": {
    "id": USER_PROFILE_URL,
    "type": "Person",
    "name": USER_NAME,
    "url": USER_PROFILE_URL,
  },
  "object": {
    "id": GROUP_URL,
    "type": "Group",
    "name": GROUP_NAME,
    "url": GROUP_URL,
    "context": {
    "id": PARENT_GROUP_URL,
    "type": "Group",
    "name": PARENT_GROUP_NAME,
    "url": PARENT_GROUP_URL,
    }
  }
}
```

