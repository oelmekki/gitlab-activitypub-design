# Activities for topic actor

## Profile

```js
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "id": TOPIC_URL,
  "type": "Group",
  "name": TOPIC_NAME,
  "url": TOPIC_URL,
  "summary": TOPIC_DESCRIPTION,
  "outbox": TOPIC_OUTBOX_URL,
  "inbox": null,
}
```

## Outbox

Like the release actor, the topic specification is very simple : it only
generates an activity when a new project has been added to the given topic.

```js
{
  "id": TOPIC_OUTBOX_URL#event_id,
  "type": "Add",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "actor": {
    "id": PROJECT_URL,
    "type": "Application",
    "name": PROJECT_NAME,
    "url": PROJECT_URL,
  },
  "object": {
    "id": TOPIC_URL,
    "type": "Group",
    "name": TOPIC_NAME,
    "url": TOPIC_URL,
    },
  },
}
```

## Possible difficulties

There is hidden complexity here.

The easy way to do this endpoint would be to take the projects associated
to a topic and sort them by descending creation date. The problem is that
if we do that, there will be some discrepancies when implementing [the
activity push part of the standard](../social-following.md#implementing-subscribing-and-pushing).

Adding the project to a topic is not made at project creation time, it's
made when a project's topics are edited. It can come after a very long time
after the project creation date. In that case, a push activity will be
created and sent to federated instances when adding the topic to the
project, but the list in the outbox endpoint sorting project by descending
creation date will not show that project, since it will have been created
long ago.

From what I've seen in the codebase, there is currently no special logic
happening when a topic is added to a project, except cleaning up the topic
list and creating the topic in database if it doesn't exist yet, there is
no event generated. We will need to add such event : that way, the activity
pushing will create an event (ideally in `Projects::UpdateService`), and
then the outbox endpoint can list those events to be sure to match what was
sent. When doing that, though, I should verify that it doesn't affect other
pages/endpoints dealing with events.
