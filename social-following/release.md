# Activities for following release actor

## Profile

The profile is this actor is a bit different from other, because it's not
that we want to show activities for a given release, what we want instead
is releases for a given project.

So the profile endpoint is handled by `Projects::ReleasesController#index`,
on the list of releases, and should reply with something like this:

```js
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "id": PROJECT_RELEASES_URL,
  "type": "Application",
  "name": PROJECT_NAME + " releases",
  "url": PROJECT_RELEASES_URL,
  "content": PROJECT_DESCRIPTION,
  "context": {
    "id": PROJECT_URL,
    "type": "Application",
    "name": PROJECT_NAME,
    "summary": PROJECT_DESCRIPTION,
    "url": PROJECT_URL,
  },
  "outbox": PROJECT_RELEASES_OUTBOX_URL,
  "inbox": null,
}
```

## Outbox

The release actor will be fairly simple: the only activity happening is the
"Create release" event.

```js
{
  "id": PROJECT_RELEASES_OUTBOX_URL#release_id,
  "type": "Create",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "actor": {
    "id": USER_PROFILE_URL,
    "type": "Person",
    "name": USER_NAME,
    "url": USER_PROFILE_URL,
  },
  "object": {
    "id": RELEASE_URL,
    "type": "Application",
    "name": RELEASE_TITLE,
    "url": RELEASE_URL,
    "content": RELEASE_DESCRIPTION,
    "context": {
      "id": PROJECT_URL,
      "type": "Application",
      "name": PROJECT_NAME,
      "summary": PROJECT_DESCRIPTION,
      "url": PROJECT_URL,
    },
  },
}
```
